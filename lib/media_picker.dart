import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import 'media.dart';
import 'media_source_options.dart';

const int IMAGE_COMPRESSION = 60;

class MediaPicker {
  static Future<LocalMedia> _pickImage({
    ImageSource source: ImageSource.gallery,
    MediaAspectRatio aspectRatio,
  }) async {
    final pickedFile = await ImagePicker().getImage(source: source);
    if (pickedFile == null) {
      return null;
    }
    final file =
        await cropImage(imagePath: pickedFile.path, aspectRatio: aspectRatio);
    final len = await file.length();
    print('Size: ${len / 1024}');
    return file == null ? null : LocalMedia.image(file);
  }

  static Future<LocalMedia> pickMedia({
    @required MediaType type,
    @required MediaSource source,
    MediaAspectRatio aspectRatio,
  }) async {
    switch (source) {
      case MediaSource.camera:
        switch (type) {
          case MediaType.image:
            return pickImageFromCamera(aspectRatio: aspectRatio);
          case MediaType.video:
            return pickVideoFromCamera();
            break;
        }
        break;
      case MediaSource.gallery:
        switch (type) {
          case MediaType.image:
            return pickImageFromGallery(aspectRatio: aspectRatio);
            break;
          case MediaType.video:
            return pickVideoFromGallery();
            break;
        }
        break;
    }
    return null;
  }

  static Future<LocalMedia> pickImageFromGallery(
      {MediaAspectRatio aspectRatio}) async {
    return _pickImage(source: ImageSource.gallery, aspectRatio: aspectRatio);
  }

  static Future<LocalMedia> pickImageFromCamera(
      {MediaAspectRatio aspectRatio}) async {
    return _pickImage(source: ImageSource.camera, aspectRatio: aspectRatio);
  }

  static Future<LocalMedia> pickProfilePicture(BuildContext context) async {
    final source = await _showMediaSourcePickerSheet(context);
    if (source == null) {
      return null;
    }
    if (source == MediaSource.camera) {
      return pickProfilePictureFromCamera();
    } else {
      return pickProfilePictureFromGallery();
    }
  }

  static Future<LocalMedia> pickProfilePictureFromCamera() async {
    return _pickImage(
      source: ImageSource.camera,
      aspectRatio: MediaAspectRatio.square(),
    );
  }

  static Future<LocalMedia> pickProfilePictureFromGallery() async {
    return _pickImage(
      source: ImageSource.gallery,
      aspectRatio: MediaAspectRatio.square(),
    );
  }

  static Future<LocalMedia> pickVideo({
    MediaSource source,
    bool cropEnabled: true,
  }) async {
    switch (source) {
      case MediaSource.camera:
        return pickVideoFromCamera();
        break;
      case MediaSource.gallery:
        return pickVideoFromGallery();
        break;
    }
    return null;
  }

  static Future<LocalMedia> pickVideoFromGallery() async {
    final file = await ImagePicker().getVideo(source: ImageSource.gallery);
    return file == null ? null : LocalMedia.video(File(file.path));
  }

  static Future<LocalMedia> pickVideoFromCamera() async {
    final file = await ImagePicker().getVideo(source: ImageSource.camera);
    return file == null ? null : LocalMedia.video(File(file.path));
  }

  static Future<File> cropImage({
    String imagePath,
    MediaAspectRatio aspectRatio,
  }) async {
    return await ImageCropper.cropImage(
      sourcePath: imagePath,
      maxHeight: 1350,
      maxWidth: 1080,
      aspectRatioPresets: aspectRatio != null
          ? []
          : [
              CropAspectRatioPreset.square,
              CropAspectRatioPreset.ratio3x2,
              CropAspectRatioPreset.original,
              CropAspectRatioPreset.ratio4x3,
              CropAspectRatioPreset.ratio16x9
            ],
      androidUiSettings: AndroidUiSettings(
        toolbarTitle: 'Edit Image',
        toolbarColor: Colors.deepOrangeAccent,
        toolbarWidgetColor: Colors.white,
        initAspectRatio: CropAspectRatioPreset.original,
        lockAspectRatio: aspectRatio != null,
      ),
      iosUiSettings: IOSUiSettings(
        minimumAspectRatio: 1.0,
        aspectRatioLockEnabled: aspectRatio != null,
      ),
      aspectRatio: aspectRatio != null
          ? CropAspectRatio(
              ratioX: aspectRatio.width, ratioY: aspectRatio.height)
          : null,
      compressQuality: IMAGE_COMPRESSION,
    );
  }
}

Future<LocalMedia> showImagePickerSheet(BuildContext context) async {
  final source = await _showMediaSourcePickerSheet(context);
  if (source == null) return null;

  return await MediaPicker.pickMedia(source: source, type: MediaType.image);
}

Future<LocalMedia> showVideoPickerSheet(BuildContext context) async {
  final source = await _showMediaSourcePickerSheet(context);
  if (source == null) return null;

  return await MediaPicker.pickMedia(source: source, type: MediaType.video);
}

Future<MediaSource> _showMediaSourcePickerSheet<MediaSource>(
  BuildContext context,
) {
  return _showAppModalBottomSheet<MediaSource>(
    context: context,
    title: 'Choose from',
    isScrollControlled: false,
    child: MediaSourceOptionContainer(),
  );
}

Future<T> _showAppModalBottomSheet<T>({
  BuildContext context,
  String title,
  Widget child,
  double heightFactor,
  bool isScrollControlled = false,
  bool showAboveKeyboard = false,
}) {
  return showModalBottomSheet<T>(
    context: context,
    useRootNavigator: true,
    isScrollControlled: isScrollControlled,
    backgroundColor: Colors.white.withOpacity(0.9),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(16.0),
        topRight: Radius.circular(16.0),
      ),
    ),
    builder: (context) {
      return Container(
        height: heightFactor != null
            ? MediaQuery.of(context).size.height * heightFactor
            : null,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Text(
                    title ?? '',
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Colors.black,
                    ),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () => Navigator.pop(context),
                ),
              ],
            ),
            if (heightFactor != null)
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(20.0),
                  child: child,
                ),
              )
            else
              Padding(
                padding: EdgeInsets.all(20.0),
                child: child,
              ),
            Container(
              height: showAboveKeyboard
                  ? MediaQuery.of(context).viewInsets.bottom
                  : 0,
            ),
          ],
        ),
      );
    },
  );
}
