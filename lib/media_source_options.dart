import 'package:flutter/material.dart';

import 'media.dart';

class MediaSourceOptionContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          TextButton(
            child: Text('Camera'),
            onPressed: () => Navigator.pop(context, MediaSource.camera),
          ),
          SizedBox(
            height: 16,
          ),
          TextButton(
            child: Text('Gallery'),
            onPressed: () => Navigator.pop(context, MediaSource.gallery),
          ),
          SizedBox(
            height: 16,
          ),
        ],
      ),
    );
  }
}
