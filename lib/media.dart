import 'dart:io';
import 'dart:typed_data';

enum MediaType { image, video }
enum MediaSource { camera, gallery }

class MediaAspectRatio {
  final double width;
  final double height;

  MediaAspectRatio({this.width, this.height});

  MediaAspectRatio.square()
      : this.width = 1,
        this.height = 1;

  double get value => width / height;
}

class LocalMedia {
  final MediaType mediaType;
  final File file;
  Uint8List thumbnailData;

  LocalMedia.image(
    this.file, {
    this.thumbnailData,
  }) : this.mediaType = MediaType.image;

  LocalMedia.video(
    this.file, {
    this.thumbnailData,
  }) : this.mediaType = MediaType.video;
}
